/**
 * Cette classe s'occupe du déplacement de la Camera.
 * Elle est déplacé par le joueur.
 */
public class PositionCamera extends GraphicObject
{
  private float damping = 0.9;
  
  /**
   * Initialise sa position de départ avec la location reçu.
   */
  public PositionCamera(PVector loc)
  {
    location = new PVector(loc.x, loc.y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    
    topSpeed = 40;
  }
  
  /**
   * Met à jour sa position.
   * Vérifie qu'elle ne dépasse pas les limites du jeu grâce à la méthode checkEdges() en envoyant ses dimensions;
   */
  public void update(float deltaTime)
  {
    velocity.add(acceleration);
    velocity.mult(damping);
    velocity.limit(topSpeed);
    location.add (velocity);
    acceleration.mult (0);
    
    super.checkEdges(width, height);
  }
  
  /**
   * Applique l'effet de caméra au jeu
   */
  public void display()
  {
    translate(-location.x, -location.y);
  }
}
