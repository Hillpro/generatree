/**
 * Cette enum permet à Meteo de savoir si la génération de nouvelles particules est nécéssaire.
 * Si nécéssaire, permet de créer la bonne sorte de particules.
 */
public enum Temperature
{
  Soleil,
  Pluie,
  Neige
}
