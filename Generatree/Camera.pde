/**
 * Singleton représentant l'instance de la Camera.
 * Elle permet de se déplacer.
 */
public static class Camera
{
  private static Camera instance = null;
  private PositionCamera position;
  
  /**
   * Constructeur de la classe.
   * Affiche à la console la création d'une nouvelle caméra.
   */
  private Camera()
  {
    print("Creating a new Singleton");
  }
  
  /**
   * Retourne l'instance de la Camera si une caméra existe déjà,
   * sinon elle crée une nouvelle Camera.
   */
  public static Camera getInstance()
  {
    if(instance == null)
    {
      synchronized(Camera.class)
      {
        if(instance == null)
        {
          instance = new Camera();
        }
      }
    }
    
    return instance;
  }
  
  /**
   * Permet de définir la PositionCamera de la caméra.
   */
  public void setPosition(PositionCamera p)
  {
    position = p;
  }
  
  /**
   * Retourne la PositionCamera de la caméra.
   */
  public PositionCamera getPosition()
  {
    return position;
  }
}
