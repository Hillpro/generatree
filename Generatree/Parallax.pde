/**
 * Cette classe contient les images qui doivent se déplacer à une vitesse différente par rapport à la caméra.
 * Permet de créer un effet de profondeur.
 */
public class Parallax
{
  private ArrayList<Image> images;
  
  /**
   * Le constructeur crée un nouvel ensemble d'images vide.
   */
  public Parallax()
  {
    images = new ArrayList<Image>();
  }
  
  /**
   * Appelle la fonction update de toutes ces images
   */
  public void update(float deltaTime)
  {
    for (Image i : images)
    {
      i.update(deltaTime);
    }
  }
  
  /**
   * Appelle la fonction d'affichage de toutes ces images.
   */
  public void display()
  {
    for (Image i : images)
    {
      i.display();
    }
  }
  
  /**
   * Permet d'ajouter une image au parallax.
   */
  public void setImage(float offset, String path, PVector loc)
  {
    images.add(new Image(offset, path,  loc));
  }
}
