/**
 * Cette classe permet l'implémentation de la stratégie dans la météo.
 * Cette interface définit les méthodes que les particules doivent implémenter.
 * Elle est utilisée pour stocker les particules dans le système de paticules.
 */
public interface IParticle
{
  /**
   * Redéfinition de la fonction présente dans GraphicObject.
   * Permet l'accès de cette fonction en dehors de la particule.
   */
  public void update(float deltaTime);
  
  /**
   * Redéfinition de la fonction présente dans GraphicObject.
   * Permet l'accès de cette fonction en dehors de la particule.
   */
  public void display();
  
  /**
   * Applique un effet de dégradé sur la pellicule en réduisant ça durée de vie.
   */
  public void fade();
  
  /**
   * Vérifie si la particule est encore visible.
   * Permet au système de particules de la supprimer si elle a disparu.
   */
  public boolean isDead();
  
  /**
   * Retourne la location actuelle de le particule.
   * Permet notamment au système de particules de vérifier la particule est en contact avec le sol.
   */
  public PVector getLocation();
  
  /**
   * Redéfinition de la fonction présente dans GraphicObject.
   * Permet l'accès de cette fonction en dehors de la particule.
   */
  public void  applyForce(PVector force);
}
