/**
 * Template méthode incluant des méthodes abstraites et concrètes.
 * Tous les objects graphiques affichés à l'écran hérite de cette classe.
 */
public abstract class GraphicObject
{
  protected PVector location;
  protected PVector velocity;
  protected PVector acceleration;
  
  protected float mass;
  protected float topSpeed;
  
  /**
   * Cette fonction est implémentée par les enfants qui hérite de cette classe.
   * Sert à mettre à jour les données de l'objet.
   */
  protected abstract void update(float deltaTime);
  
  /**
   * Cette fonction est implémentée par les enfants qui hérite de cette classe.
   * Sert à afficher l'objet.
   */
  protected abstract void display();
  
  /**
   * Cette fonction vérifie si l'object rentre dans les limites définies du jeu.
   * Retourne vrai si l'object a atteint une limite du jeu.
   * Ajuste ça position pour revenir dans le jeu.
   */
  public boolean checkEdges(float w, float h) 
  {
    boolean onEdge = false;
    
    if (location.x < 0) 
    {
      location.x = 0;
      onEdge = true;
    } 
    else if (location.x > gameSize.x - w) 
    {
      location.x = gameSize.x - w;
      onEdge = true;
    }
    
    if (location.y < 0) 
    {
      location.y = 0 ;
      onEdge = true;
    } 
    else if (location.y > gameSize.y - h) 
    {
      location.y = gameSize.y - h;
      onEdge = true;
    }
    
    return onEdge;
  }
  
  /**
   * Cette fonction applique la force reçu en paramètre à l'accélération de l'objet.
   */
  public void applyForce(PVector force)
  {
    //PVector f = force.get();
    acceleration.add(force);
  }
}
