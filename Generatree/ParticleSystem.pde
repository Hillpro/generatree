/**
 * Est l'ensemble de particules que contient la classe Meteo.
 * Peut contenir différents types de particules en même temps.
 */
public class ParticleSystem
{
  private ArrayList<IParticle> particles;
  private Terrain terrain;
  
  /**
   * Crée une nouvelle liste de particules.
   * Garde le Terrain en référence pour gérer les collisions avec ses particules.
   */
  public ParticleSystem(Terrain t)
  {
    particles = new ArrayList<IParticle>();
    
    this.terrain = t;
  }
  
  /**
   * Commence par mettre à jour toutes ses particules.
   * Vérifie ensuite si la particule touche au terrain et réduit sa durée de vie si nécéssaire.
   * Retire les particules qui ne sont plus visibles.
   */
  public void update()
  {
    for(int i = particles.size()-1; i >= 0; i--)
    {
      IParticle p = particles.get(i);
      p.update(deltaTime);
      
      if(terrain.isColliding(p))
      {
        p.fade();
      }
      
      if(p.isDead())
      {
        particles.remove(i);
      }
    }
  }
  
  /**
   * Affiche toutes ses particules.
   */
  public void display()
  {
    for (IParticle p : particles)
    {
      p.display();
    }
  }
  
  /**
   * Permet d'ajouter une particule au système.
   * Utilisé par la classe Meteo.
   */
  public void setParticle(IParticle p)
  {
    particles.add(p);
  }
  
  /**
   * Appelle la fonction applyForce de ses particules, dans GraphicObject.
   */
  public void applyForce(PVector force)
  {
    for (IParticle p : particles)
    {
      p.applyForce(force);
    }
  }
}
