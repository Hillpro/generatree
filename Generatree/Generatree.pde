private int currentTime;
private int previousTime;
private int deltaTime;

private PVector gameSize;
private float gameSizeCoef = 2;

private Camera camera;
private float cameraSpeed = 2;

private Parallax parallax;
private Terrain terrain;

private Meteo meteo;

private PVector gravity;
private PVector wind;


void setup()
{
  fullScreen(P3D);
  hint(DISABLE_DEPTH_TEST);
  //hint(DISABLE_OPTIMIZED_STROKE);
  frameRate(60);
  currentTime = millis();
  previousTime = millis();
  
  gameSize = new PVector(width*gameSizeCoef, height*gameSizeCoef);
  
  camera = Camera.getInstance();
  camera.setPosition(new PositionCamera(new PVector(gameSize.x/2-width/2, gameSize.y-height)));
  
  parallax = new Parallax();
  
  parallax.setImage(0.3, "Background.jpg", new PVector(0, 0));
  
  terrain = new Terrain();
  
  meteo = new Meteo(Temperature.Soleil, terrain);
  
  gravity = new PVector(0, 0.1);
  wind = new PVector(0.05, 0);
}

void draw()
{
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  
  update(deltaTime);
  display();
  
  previousTime = currentTime;
}

void update(int deltaTime)
{
  if(keyPressed)
  {
    if(key== 'a')
        camera.getPosition().applyForce(new PVector(-cameraSpeed, 0));
    else if (key == 'd')
        camera.getPosition().applyForce(new PVector(cameraSpeed, 0));
    else if(key== 'w')
        camera.getPosition().applyForce(new PVector(0, -cameraSpeed));
    else if (key == 's')
        camera.getPosition().applyForce(new PVector(0, cameraSpeed));
  }
  
  if (mousePressed) 
  {
    if(mouseButton == RIGHT)
    {
      meteo.getSystem().applyForce(wind);
    }
    else if(mouseButton == LEFT)
    {
      meteo.getSystem().applyForce(new PVector(-wind.x, 0));
    }
  }
    
  
  camera.getPosition().update(deltaTime);
  
  parallax.update(deltaTime);
  
  terrain.update(deltaTime);
  
  meteo.update();
}

void display()
{
  background(125, 125, 125);
  
  parallax.display();
  
  text("P : Pluie", width-100, 40);
  text("N : Neige", width-100, 80);
  text("B : Soleil", width-100, 120);
  text("R : Redémarrer", width-100, 160);
  
  pushMatrix();
  
  camera.getPosition().display();
  
  terrain.display();
  
  meteo.getSystem().display();
  
  popMatrix();
}

void keyPressed()
{
  switch (key)
  {
    case 'p' : meteo.setTemperature(Temperature.Pluie);
      break;
      
    case 'n' : meteo.setTemperature(Temperature.Neige);
      break;
      
    case 'b' : meteo.setTemperature(Temperature.Soleil);
      break;
      
    case 'r' :  camera.setPosition(new PositionCamera(new PVector(gameSize.x/2-width/2, gameSize.y-height)));
                
                terrain = new Terrain();
                
                meteo = new Meteo(Temperature.Soleil, terrain);
      break;
  }
}
