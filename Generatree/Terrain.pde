/**
 * Cette classe crée le terrain avec lequel les IParticle peuvent rentrer en collision.
 * Elle génère le terrain grâce à du bruit de Perlin.
 */ 
public class Terrain extends GraphicObject
{
  private  PVector[] sol;
  private float xoff;
  private float yoff;
  
  /**
   * Le constructeur génère le terrain point par point en fonction de la taille du jeu.
   */
  public Terrain()
  {
    yoff = height*0.7;
    xoff = random(1000);
    
    sol = new PVector[int(gameSize.x + width)];
    
    for (int i = 0; i < int(gameSize.x + width); i++)
    {
      xoff = xoff + .003;
      sol[i] = new PVector();
      sol[i].x = i;
      sol[i].y = (noise(xoff)*(height/4)) + yoff;
    }
    
    location = new PVector(0, 0);
  }
  
  /**
   * La fonction de mise à jour s'assure seulement que la position du terrain reste cohérente.
   */
  public void update(float deltaTime)
  {
    location.x = 0;
    location.y = gameSize.y - height;
  }
  
  /**
   * Affiche le terrain à l'aide des points du sol et de lignes vers le bas.
   */
  public void display()
  {
    for (PVector p : sol)
    {
      stroke(55, 100, 30);
      
      line(p.x + location.x, p.y + location.y, p.x + location.x,  p.y + location.y + (height-yoff));
      
      stroke(75);
      
      point(p.x + location.x, p.y + location.y);
    }
  }
  
  /**
   * Cette fonction est appellé pas le ParticleSystem.
   * Elle permet de vérifie si une particule touche au terrain.
   */
  public boolean isColliding(IParticle p)
  {
    if(p.getLocation().x >= 0 && p.getLocation().x < gameSize.x + width)
    {
      if(p.getLocation().y >= sol[int(p.getLocation().x)].y + location.y)
      {
        return true;
      }
    }
    
    return false;
  }
}
