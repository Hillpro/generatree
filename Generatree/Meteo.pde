/**
 * Cette classe fait la gestion de la météo.
 * C'est elle qui possède la condition météorologique actuelle ainsi que le système de particule
 */
public class Meteo
{
  private Temperature temperature;
  private ParticleSystem system;
  
  /**
   * Constructeur de la classe.
   * Création d'un nouveau système de particule avec le terrain actuel.
   * Définition de la température de départ.
   */
  public Meteo(Temperature t, Terrain terrain)
  {
    system = new ParticleSystem(terrain);
    
    temperature = t;
  }
  
  /**
   * Pendant la mise à jour de cette classe, celle-ci crée de nouvelle particule si nécessaire.
   * Elle appelle ensuite le system de particules à ce mettre à jour.
   */
  public void update()
  {
    if(temperature == Temperature.Pluie)
    {
      for(int i = 0; i <25; i ++)
      {
        system.setParticle(new Pluie());
      }
    }
    else if (temperature == Temperature.Neige)
    {
      for(int i = 0; i <25; i ++)
      {
        system.setParticle(new Neige());
      }
    }
    
    system.applyForce(gravity);
    system.update();
  }
  
  /**
   * Retourne son système de particules.
   */
  public ParticleSystem getSystem()
  {
    return system;
  }
  
  /**
   * Permet de définir une nouvelle température.
   */
  public void setTemperature(Temperature t)
  {
    temperature = t;
  }
}
