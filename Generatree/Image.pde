/**
 * Cette classe permet de contenir les images du parallax.
 * Encapsule la taille et le facteur de déplacement avec l'image.
 */
public class Image extends GraphicObject
{
  private PImage image;
  private float offset;
  private Camera camera;
  private PVector size;
  
  /**
   * Le construceur crée une nouvelle image.
   * Elle initialise son facteur de déplacement, son image et sa taille.
   */
  public Image(float offset, String path, PVector loc)
  {
    this.location = loc;
    image = loadImage(path);
    this.offset = offset;
    
    size = new PVector(width + ((gameSize.x - width) * offset), height + ((gameSize.y - height) * offset));
    
    this.camera = Camera.getInstance();
  }
  
  /**
   * Met à jour la position de l'image par rapport à la position de la caméra et de son facteur de déplacement.
   */
  public void update(float deltaTime)
  {
    location.x = -camera.getPosition().location.x * offset;
    location.y = -camera.getPosition().location.y * offset;
  }
  
  /**
   * Affiche l'image du parallax
   */
  public void display()
  {
    image(image, location.x, location.y, size.x, size.y);
  }
}
