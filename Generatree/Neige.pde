/**
 * Cette classe hérite de GraphicObject et implémente IParticle.
 * Représente une particule de neige.
 */
public class Neige extends GraphicObject implements IParticle
{
  private float lifespan;
  private float radius = 5;
  
  /**
   * Crée une nouvelle particule de neige a une position aléatoire.
   */
  public Neige()
  {
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, random(-1, 1));
    location = new PVector(random(-width, gameSize.x+width), -height/2);
    lifespan = 256;
  }
  
  /**
   * Met à jour la position de la particule.
   * Vérifie si la particule est sortie de la limite inférieure.
   */
  public void update(float deltaTime)
  {
    velocity.add(acceleration);
    location.add(velocity);
    
    if(location.y>gameSize.y)
      lifespan = 0;
    
    acceleration.mult(0);
  }
  
  /**
   * Affiche la particule de neige à ça position actuelle.
   */
  public void display()
  {
    pushMatrix();
      fill(255, lifespan);
      translate(location.x, location.y);
      rect(0, 0, radius, radius);
    popMatrix();
  }
  
  /**
   * Applique un effet de dégradé sur la pellicule en réduisant ça durée de vie.
   */
  public void fade()
  {
    lifespan -= 40;
  }
  
  /**
   * Retourne la location actuelle de le particule.
   * Permet notamment au système de particules de vérifier la particule est en contact avec le sol.
   */
  public PVector getLocation()
  {
    return location; 
  }
  
  /**
   * Vérifie si la particule est encore visible.
   * Permet au système de particules de la supprimer si elle a disparu.
   */
  public boolean isDead()
  {
    if(lifespan <= 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
