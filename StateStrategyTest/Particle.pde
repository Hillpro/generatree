public interface Particle
{
  public void update(float deltaTime);
  
  public void display();
  
  public boolean isDead();
  
  public void  applyForce(PVector force);
}
