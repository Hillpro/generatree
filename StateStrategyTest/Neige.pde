public class Neige extends GraphicObject implements Particle
{
  float lifespan;
  
  public Neige()
  {
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, random(-1, 1));
    location = new PVector(random(-width, width*2), -height*2);
    lifespan = 100;
  }
  
  void update(float deltaTime)
  {
    velocity.add(acceleration);
    location.add(velocity);
    
    if(location.y>height)
      lifespan = 0;
    
    acceleration.mult(0);
  }
  
  boolean isDead()
  {
    if(lifespan <= 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  void display()
  {
    pushMatrix();
      stroke(0, lifespan);
      strokeWeight(1);
      translate(location.x, location.y);
      //rotate(-radians(90*velocity.x/velocity.y));
      //line(0, 0, 0, -7);
      rect(0, 0, 5, 5);
    popMatrix();
  }
}
