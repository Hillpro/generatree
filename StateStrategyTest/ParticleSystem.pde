public class ParticleSystem
{
  ArrayList<Particle> particles;
  
  ParticleSystem()
  {
    particles = new ArrayList<Particle>();
  }
  
  void update()
  {
    for(int i = particles.size()-1; i >= 0; i--)
    {
      Particle p = particles.get(i);
      p.update(deltaTime);
      
      if(p.isDead())
      {
        particles.remove(i);
      }
    }
  }
  
  void setParticle(Particle p)
  {
    particles.add(p);
  }
  
  void applyForce(PVector force)
  {
    for (Particle p : particles)
    {
      p.applyForce(force);
    }
  }
  
  void display()
  {
    for (Particle p : particles)
    {
      p.display();
    }
    
    text(particles.size(), 100, 100);
  }
}
