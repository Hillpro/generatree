public class Pluie extends GraphicObject implements Particle
{
  float lifespan;
  
  public Pluie()
  {
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, random(-1, 1));
    location = new PVector(random(-width*1.5, width*2.5), -height*2.5);
    lifespan = 256;
  }
  
  void update(float deltaTime)
  {
    velocity.add(acceleration);
    location.add(velocity);
    
    if(location.y>height)
      lifespan = 0;
    
    acceleration.mult(0);
  }
  
  boolean isDead()
  {
    if(lifespan <= 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  void display()
  {
    pushMatrix();
      stroke(0, lifespan);
      strokeWeight(2);
      translate(location.x, location.y);
      rotate(-radians(90*velocity.x/velocity.y));
      line(0, 0, 0, -7);
    popMatrix();
  }
}
