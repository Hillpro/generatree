public class ParticleSystem
{
  ArrayList<Particle> particles;
  
  ParticleSystem()
  {
    particles = new ArrayList<Particle>();
  }
  
  void update()
  {
    for(int i = 0; i <25; i ++)
    {
      particles.add(new Particle());
    }
    
    for(int i = particles.size()-1; i >= 0; i--)
    {
      Particle p = particles.get(i);
      p.update(deltaTime);
      
      if(p.isDead())
      {
        particles.remove(i);
      }
    }
  }
  
  void applyForce(PVector force)
  {
    for (Particle p : particles)
    {
      p.applyForce(force);
    }
  }
  
  void display()
  {
    for (Particle p : particles)
    {
      p.display();
    }
    
    text(particles.size(), 100, 100);
  }
}
