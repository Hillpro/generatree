abstract class GraphicObject
{
  PVector location;
  PVector velocity;
  PVector acceleration;
  
  color fillColor = color(255);
  
  float mass;
  float topSpeed;
  
  boolean visible;
  
  abstract void update(float deltaTime);
  
  abstract void display();
}
