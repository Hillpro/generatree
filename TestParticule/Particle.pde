public class Particle extends GraphicObject
{
  float lifespan;
  //float maxWind = 10;
  
  public Particle()
  {
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, random(-1, 1));
    location = new PVector(random(-width*1.5, width*2.5), -height*2.5);
    lifespan = 100;
  }
  
  void update(float deltaTime)
  {
    velocity.add(acceleration);
    
    /*if(velocity.x >maxWind)
    {velocity.x = maxWind;}
    else if(velocity.x <-maxWind)
    {velocity.x = -maxWind;}*/
    
    location.add(velocity);
    
    if(location.y>height)
    {lifespan = 0;}
    
    acceleration.mult(0);
    //lifespan -= 1;
  }
  
  void applyForce(PVector force)
  {
    acceleration.add(force);
  }
  
  boolean isDead()
  {
    if(lifespan <= 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  void display()
  {
    pushMatrix();
      stroke(0, lifespan);
      strokeWeight(1);
      translate(location.x, location.y);
      rotate(-radians(90*velocity.x/velocity.y));
      line(0, 0, 0, -7);
    popMatrix();
  }
}
