int currentTime;
int previousTime;
int deltaTime;

ParticleSystem system;
PVector wind;
PVector gravity;

void setup()
{
  size(800,600);
  currentTime = millis();
  previousTime = millis();
  //frameRate(60);
  
  gravity = new PVector(0, 0.1);
  wind = new PVector(0.05, 0);
  
  system = new ParticleSystem();
}

void draw()
{
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  
  update(deltaTime);
  display();
  
  previousTime = currentTime;
}

void update(int deltaTime)
{
  system.applyForce(gravity);
  
  if (mousePressed && (mouseButton == RIGHT)) 
  {
    system.applyForce(wind);
  } 
  else if (mousePressed && (mouseButton == LEFT)) 
  {
    system.applyForce((new PVector(-wind.x, 0)));
  }
  
  system.update();
}

void display()
{
  background(255);
  
  system.display();
}

void mousePressed()
{
  switch (mouseButton)
  {
    case  RIGHT:
    
      break;
    
    case LEFT:
    
      break;
  }
}

void mouseReleased()
{
  
}

void keyPressed()
{
  
  switch (key)
  {
    case 'p' : while(true){}
  }
}
