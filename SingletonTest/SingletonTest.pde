int currentTime;
int previousTime;
int deltaTime;

Camera camera;

void setup()
{
  size(800,600);
  currentTime = millis();
  previousTime = millis();
  
  camera = Camera.getInstance();
  Position p = new Position(new PVector(width-width/2, height-height/2));
  camera.setPosition(p);
  
  print(camera.getPosition().location);
  
}

void draw()
{
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  
  update(deltaTime);
  display();
  
  previousTime = currentTime;
}

void update(int deltaTime)
{
  
}

void display()
{
  background(125, 125, 125);
  
}

void mousePressed()
{
  switch (mouseButton)
  {
    case  RIGHT:
    
      break;
    
    case LEFT:
    
      break;
  }
}

void mouseReleased()
{
  
}

void keyPressed()
{
  
  switch (key)
  {
    
  }
}
