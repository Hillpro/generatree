public static class Camera
{
  private static Camera instance = null;
  private Position position;
  
  private Camera()
  {
    
  }
  
  public static Camera getInstance()
  {
    if(instance == null)
    {
      instance = new Camera();
    }
    
    return instance;
  }
  
  public void setPosition(Position p)
  {
    position = p;
  }
  
  public Position getPosition()
  {
    return position;
  }
}
