class Position extends GraphicObject
{
  float topSpeed = 40;
  float damping = 0.9;
  
  public Position(PVector loc)
  {
    location = new PVector(loc.x, loc.y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
  }
  
  void update(float deltaTime)
  {
    velocity.add(acceleration);
    velocity.mult(damping);
    velocity.limit(topSpeed);
    location.add (velocity);
    acceleration.mult (0);
    
    super.checkEdges(width, height);
  }
  
  void display()
  {
    translate(-location.x, -location.y);
  }
}
