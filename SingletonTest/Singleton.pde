public static class Singleton 
{
    private static Singleton instance = null;

    private Singleton() 
    {
      print("Hi");
    }

    public static Singleton getInstance()
    {
        if(instance == null)
        {
          instance = new Singleton();
        }
        
        print("There");
        
        return instance;
    } //<>// //<>//

}
