abstract class GraphicObject
{
  protected PVector location;
  protected PVector velocity;
  protected PVector acceleration;
  
  protected color fillColor = color(255);
  
  protected float mass;
  protected float topSpeed;
  
  protected boolean visible;
  
  protected abstract void update(float deltaTime);
  
  protected abstract void display();
  
  public void checkEdges(float w, float h) {
    if (location.x < 0) {
      location.x = 0;
    } else if (location.x > width - w) {
      location.x = width - w;
    }
    
    if (location.y < 0) {
      location.y = 0 ;
    } else if (location.y > height - h) {
      location.y = height - h;
    }
  }
  
  public void applyForce(PVector force)
  {
    //PVector f = force.get();
    acceleration.add(force);
  }
}
