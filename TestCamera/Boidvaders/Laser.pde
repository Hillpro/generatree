class Laser extends GraphicObject
{
  float theta=0;
  
  int topSpeed = 10;
  int speed = 8;
  int size = 10;
  
  Vaisseau v;
  
  Laser(PVector loc, float theta, Vaisseau v)
  {
    this.location = loc;
    this.velocity = new PVector(0, 0);
    this.acceleration = new PVector (0 , 0);
    
    velocity.x = -sin(theta);
    velocity.y = cos(theta);
    
    velocity.mult(speed);
    
    fillColor = color(255, 0, 0);
    
    visible = true;
    this.theta = theta;
    
    this.v = v;
  }
  
  void update(float deltaTime)
  {
    checkEdges();
    
    velocity.add (acceleration);
    velocity.limit(topSpeed);
    

    location.add (velocity);

    acceleration.mult (0);
  }
  
  void display()
  {
    noStroke();
    
    pushMatrix();
    translate(location.x, location.y);
    rotate (theta);
    
    fill(fillColor);
    
    beginShape(QUADS);
      vertex(0, -5);
      vertex(0, 30);
      vertex(10, 30);
      vertex(10, -5);
    endShape();
    
    popMatrix();
  }
  
  void checkEdges() {
    if (location.x < 0) {
      this.visible = false;
      v.nombreLasers--;
    } else if (location.x + size> width) {
      this.visible = false;
      v.nombreLasers--;
    }
    
    if (location.y < 0) {
      this.visible = false;
      v.nombreLasers--;
    } else if (location.y + size> height) {
      this.visible = false;
      v.nombreLasers--;
    }
  }
  
  boolean isColliding(ArrayList<Boids> flock)
  {
    
    for (Boids b : flock) {
    if(b.visible)
    {
       if(this.size + b.r > dist(this.location.x, this.location.y, b.location.x, b.location.y))
      {
        b.visible = false;
        this.visible = false;
        v.nombreLasers--;
        
        return true;
      }
      
      }
    }
    
    return false;
  }
   
}
