class Vaisseau extends GraphicObject
{
  float data = 0;
  
  float topSpeed = 3;
  float speedAcceleration = 0;
  float heading = 0;
  float damping = 0.999;
  
  float mass = 5;
  
  float r = 50;
  
  color colorCanon;
  
  ArrayList<Laser> lasers;
  int nombreLasers = 0;
  int maxLaser = 5;
  int delay = 0;
  
  char side= 'L';
  
  Vaisseau(PVector loc, PVector vel, ArrayList<Laser> lasers)
  {
    this.location = loc;
    this.velocity = vel;
    this.acceleration = new PVector (0 , 0);
    this.lasers = lasers;
    
    fillColor = color(0, 255, 0);
    colorCanon = color(0, 0, 255);
    
    visible = true;
  }
  
  void checkEdges() {
    if (location.x < 0) {
      location.x = width - r;
    } else if (location.x + r> width) {
      location.x = 0;
    }
    
    if (location.y < 0) {
      location.y = height - r;
    } else if (location.y + r> height) {
      location.y = 0;
    }
  }
  
  void update(float deltaTime)
  {
    //checkEdges();
    
    velocity.add(acceleration);
    velocity.mult(damping);
    velocity.limit(topSpeed);
    location.add (velocity);
    acceleration.mult (0);
    delay -= deltaTime;
  }
  
  void applyForce(PVector force)
  {
    PVector f = force.get();
    acceleration.add(f);
  }
  
  void display()
  {
    noStroke();
    
    pushMatrix();
    translate(location.x, location.y);
    rotate (heading);
    
    fill (colorCanon);
    
    beginShape(QUADS);
      vertex(-20, -5);
      vertex(-20, 30);
      vertex(-5, 30);
      vertex(-5, -5);
      vertex(5, -5);
      vertex(5, 30);
      vertex(20, 30);
      vertex(20, -5);
    endShape();
    
    fill (fillColor);
    
    ellipse(0, 0, r, r);
    
    popMatrix();
  }
  
  void shoot()
  {
    
    if(delay<0 && nombreLasers<5)
    {
      nombreLasers++;
      
      if(side == 'L')
      {
        lasers.add(new Laser(new PVector(location.x, location.y - 18), heading, this));
        side = 'R';
      }
      else
      {
        lasers.add(new Laser(new PVector(location.x, location.y + 8), heading, this));
        side = 'L';
      }
      
      delay = 100;
    }
  }
  
  void turn(float angle)
  {
    heading += angle;
  }
  
  void avance() {
    float angle = heading - PI/2;
    PVector force = new PVector(cos(angle),sin(angle));
    force.mult(0.1);
    applyForce(force);
  }
  
  boolean isColliding(Boids b)
  {
    if(this.r + b.r*0.6 > dist(this.location.x, this.location.y, b.location.x, b.location.y))
    {
      visible = false;
      
      return true;
    }
    
    return false;
  }
}
