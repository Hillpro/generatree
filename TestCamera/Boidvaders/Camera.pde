public class Camera
{
  private float x, y;
  
  public Camera(float x, float y)
  {
    this.x = x;
    this.y = y;
  }
  
  void update(float deltaTime, Vaisseau v)
  {
    x = -v.location.x + width/2;
    y = -v.location.y + height/2;
  }
  
  public float getX()
  {
    return x;
  }
  
  public float getY()
  {
    return y;
  }
}
