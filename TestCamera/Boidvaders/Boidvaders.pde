//Rémi Contet

int currentTime;
int previousTime;
int deltaTime;

ArrayList<Boids> flock;
int flockSize = 50;
int flockLeft = 0;

Vaisseau vaisseau;
PVector departBoids;

ArrayList<Laser> lasers;
float tierDiagonal = 0;

Camera camera;
Parallax p;

PImage image;

void setup()
{
  fullScreen(P2D);
  currentTime = millis();
  previousTime = millis();
  
  image = loadImage("test2.jpg");
  
  flock = new ArrayList<Boids>();
  lasers = new ArrayList<Laser>();
  
  p = new Parallax();
  
  camera = new Camera(0, 0);
  
  vaisseau = new Vaisseau(new PVector(width/2, height/2), new PVector(0, 0), lasers);
  
  tierDiagonal = (sqrt((width*width)+(height*height)))/3;
  
  do
  {
    departBoids = new PVector(random(0, width), random(0, height));
  }while(tierDiagonal > dist(departBoids.x, departBoids.y, vaisseau.location.x, vaisseau.location.y));
  
  for (int i = 0; i < flockSize; i++) {
    Boids b = new Boids(new PVector(departBoids.x, departBoids.y), new PVector(random (-2, 2), random(-2, 2)));
    b.fillColor = color(random(255), random(255), random(255));
    flock.add(b);
  }
}

void draw()
{
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  
  update(deltaTime);
  display();
  
  previousTime = currentTime;
}

void update(int deltaTime)
{
  for (Boids b : flock) {
    if(b.visible)
    {
      b.flock(flock);
      b.update(deltaTime);
      vaisseau.isColliding(b);
    }
  }
  
  for (Laser l : lasers) {
    if(l.visible)
    {
      l.update(deltaTime);
      
      l.isColliding(flock);
    }
  }
  
  if(keyPressed)
  {
    if(key== 'a')
        vaisseau.turn(-0.03);
    else if (key == 'd')
        vaisseau.turn(0.03);
    else if (key == 'w')
        vaisseau.avance();
  }
  
  if(vaisseau.visible)
  {
  vaisseau.update(deltaTime);
  camera.update(deltaTime, vaisseau);
  }
}

void display()
{
  background(0);
  
  pushMatrix();
  
  p.display();
  
  translate(camera.getX(), camera.getY());
  
  for (Boids b : flock) {
    if(b.visible)
    {
      b.display();
    }
    
  }
  
  flockLeft = 50;
  
  for (Boids b : flock) {
    if(b.visible == false)
    {
      flockLeft--;
    }
  }
  
  textSize(16);
  text(flockLeft + " / 50", width-100, 50);
  
  if(flockLeft < 1)
  {
    textSize(72);
    text("YOU WIN", 150, 150);
  }
  else if(vaisseau.visible == false)
  {
    textSize(72);
    text("YOU LOSE ", 150, 150);
  }
  
  for (Laser l : lasers) {
    if(l.visible)
    {
      l.display();
    }
  }
  
  if(vaisseau.visible)
  {
    vaisseau.display();
  }
  
  popMatrix();
}

void mousePressed()
{
  switch (mouseButton)
  {
    case  RIGHT:
    
      break;
    
    case LEFT:
    
      break;
  }
}

void mouseReleased()
{
  
}

void keyPressed()
{
  
  switch (key)
  {
    case ' ':
      if(vaisseau.visible)
      {
        vaisseau.shoot();
      }
      break;
      
    case 'r':
      flock = new ArrayList<Boids>();
      lasers = new ArrayList<Laser>();
      
      vaisseau = new Vaisseau(new PVector(width/2, height/2), new PVector(0, 0), lasers);

      do
      {
        departBoids = new PVector(random(0, width), random(0, height));
      }while(tierDiagonal > dist(departBoids.x, departBoids.y, vaisseau.location.x, vaisseau.location.y));
      
      for (int i = 0; i < flockSize; i++) {
        Boids b = new Boids(new PVector(departBoids.x, departBoids.y), new PVector(random (-2, 2), random(-2, 2)));
        b.fillColor = color(random(255), random(255), random(255));
        flock.add(b);
      }
      
      flockLeft = 0;
      break;
  }
}

void keyReleased()
{
  
}
