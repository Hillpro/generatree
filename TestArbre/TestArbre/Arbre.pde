public class Arbre extends GraphicObject{
  
  String sequenceArbre = "";
  ArrayList<Character> sequence;
  String axiom = "";
  
  String[] rule;
  Branche tronc;
  
  float angle;
  
  Arbre(String axiom, PVector location)
  {
    rule = new String[2];
    rule[0] = "A";
    rule[1] = "AA+(+A-A-A)"; ///-(-A+A+A)
    
    this.axiom = axiom;
    sequenceArbre = axiom;
    angle = 180;
    
    this.location = location;
    
    tronc = new Branche(0);
    
  }
  
  void update(float deltaTime)
  {
    
  }
  
  void display()
  {
    pushMatrix();  
      translate(location.x, location.y);
      rotate(radians(angle));
      stroke(127);
      strokeWeight(1);
      tronc.display();
    popMatrix();
  }
  
  public void build()
  {
    ArrayList<Character> sequence = new ArrayList<Character>();
    
    for(int i = sequenceArbre.length()-1; i >= 0; i--)
    {
      sequence.add(sequenceArbre.charAt(i));
    }
    
    tronc.build(sequence);
    
    println(tronc.branches);
    
    println(tronc.branches.get(1).noeuds);
  }
  
  public void generate()
  {
    String nextSentence = "";
    
    for(int i = 0; i < sequenceArbre.length(); i++)
    {
      
      if(sequenceArbre.charAt(i) == rule[0].charAt(0))
      {
        nextSentence += rule[1];
      }
      else
      {
        nextSentence += sequenceArbre.charAt(i);
      }
    }
    
    sequenceArbre = nextSentence;
    
    build();
  }
  
}
