int currentTime;
int previousTime;
int deltaTime;

boolean test;
Arbre arbre;

void setup()
{
  fullScreen(P2D);
  currentTime = millis();
  previousTime = millis();
  
  arbre = new Arbre("A", new PVector(width/2, height));
}

void draw()
{
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  
  update(deltaTime);
  display();
  
  previousTime = currentTime;
}

void update(int deltaTime)
{
}

void display()
{
  background(75);
  
  arbre.display();
  
  text(arbre.sequenceArbre, 100 , 100);
}

void mousePressed()
{
  switch (mouseButton)
  {
    case  RIGHT:
        
      break;
    
    case LEFT:
      arbre.generate();
      break;
  }
}

void mouseReleased()
{
  
}

void keyPressed()
{
  
  switch (key)
  {
    
  }
}
