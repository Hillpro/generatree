public class Noeud extends GraphicObject{
  
  float angle;
  ArrayList<Branche> branches;
  ArrayList<Noeud> noeuds;
  
  public Noeud(float angle)
  {
    this.angle = angle;
    branches = new ArrayList<Branche>();
    noeuds = new ArrayList<Noeud>();
  }
  
  void update(float deltaTime)
  {
    
  }
  
  void display()
  {
    for(Noeud n : noeuds)
    {
      n.display();
    }
    
    pushMatrix();
    
    for(Branche b: branches)
    {
        rotate(radians(b.angle));
        line(0, 0, 0, 100);
        translate(0, 100);
      
          for(Noeud n : b.noeuds)
          {
            n.display();
          }
    }
    
    popMatrix();
  }
  
  boolean build(ArrayList<Character> sequence)
  {
    float angleCalcul = angle;
    
    for(int i = sequence.size()-1; i >= 0; i--)
    {
      char c = sequence.get(i);
      
      if(c == 'A')
      {
          branches.add(new Branche(angleCalcul));
          sequence.remove(i);
      }
      else if(c == '+')
      {
        angleCalcul+= 20;
        
        if(angleCalcul>360) angleCalcul-=360;
        
        sequence.remove(i);
      }
      else if(c == '-')
      {
        angleCalcul-= 20;
        
        if(angleCalcul<-360) angleCalcul+=360;
        
        sequence.remove(i);
      }
      else if(c == '(')
      {
        sequence.remove(i);
        
        if(branches.size() == 0)
        {
          noeuds.add(new Noeud(angleCalcul));
          noeuds.get(noeuds.size()-1).build(sequence);
        }
        else
        {
          branches.get(branches.size()-1).noeuds.add(new Noeud(angleCalcul));
          branches.get(branches.size()-1).noeuds.get(branches.get(branches.size()-1).noeuds.size()-1).build(sequence);
        }
        
        i = sequence.size()-1;
      }
      else if(c == ')')
      {
        sequence.remove(i);
        print(this + " HAVE " + branches);
        return true;
      }
      else
      {
        println("Erreur de caractère dans la séquence");
      }
    }
    print(this + " HAVE " + branches);
    println("Null End");
    return false;
  }
}
