int currentTime;
int previousTime;
int deltaTime;

PVector[] terrain;
float xoff = 0.0;

void setup()
{
  size(800,600);
  currentTime = millis();
  previousTime = millis();
  
  terrain = new PVector[width];
  
  for (int i = 0; i < width; i++)
  {
    xoff = xoff + .003;
    terrain[i] = new PVector();
    terrain[i].x = i;
    terrain[i].y = (noise(xoff)*(height/4)) + height*0.70;
  }
}

void draw()
{
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  
  update(deltaTime);
  display();
  
  previousTime = currentTime;
}

void update(int deltaTime)
{
  
}

void display()
{
  background(125, 125, 125);
  
  for (PVector p : terrain)
  {
    stroke(55, 100, 30);
    
    line(p.x, p.y, p.x, height);
    
    stroke(75);
    
    point(p.x, p.y);
  }
}

void mousePressed()
{
  switch (mouseButton)
  {
    case  RIGHT:
    
      break;
    
    case LEFT:
    
      break;
  }
}

void mouseReleased()
{
  
}

void keyPressed()
{
  
  switch (key)
  {
    
  }
}
