int currentTime;
int previousTime;
int deltaTime;

boolean saveVideo = false;

Leaf leaf;
int diametre = 50;


void setup () {
  size (800, 600);
  currentTime = millis();
  previousTime = millis();
  
  leaf = new Leaf(width / 2, 0);
  
  leaf.velocity.x = 0;
  leaf.velocity.y = 1.5;
}



void draw () {
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  previousTime = currentTime;

  
  update(deltaTime);
  display();
  
  savingFrames(5, deltaTime);  
}

/***
  The calculations should go here
*/

int dir = 1;

float xOff = 0;
float yOff = 0;
float angle = 0;

void update(int delta) {
  
  //xOff = (xOff + 1) % diametre;
  xOff = cos (angle);
  yOff = sin (angle);
  
  angle += PI/180.0;
    
  if (leaf.dimensions.y > 20 || leaf.dimensions.y < -20) {
    dir = -dir;
  }
  
  if (leaf.dimensions.y > 0) {
    leaf.fillColor = color(127);
  } else {
    leaf.fillColor = color(255);
  }
  
  
  leaf.dimensions.y += dir;
  leaf.update(delta);
}

/***
  The rendering should go here
*/
void display () {
  //background(0);
  leaf.display();
}

//Saving frames for video
//Put saveVideo to true;
int savingAcc = 0;
int nbFrames = 0;

void savingFrames(int forMS, int deltaTime) {
  
  if (!saveVideo) return;
  
  savingAcc += deltaTime;
  
  if (savingAcc < forMS) {
    saveFrame("frames/####.tiff");
	nbFrames++;
  } else {
	println("Saving frames done! " + nbFrames + " saved");
    saveVideo = false;
  }
}
