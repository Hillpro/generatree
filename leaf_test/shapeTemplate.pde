// Rename ShapeTemplate to whatever needed
class Leaf extends GraphicObject {
  
  PVector dimensions;
  
  Leaf () {
    instanciate();
  }
  
  Leaf (float x, float y) {
    instanciate();
    location.x = x;
    location.y = y;
    
  }
  
  
  
  void instanciate() {
    location = new PVector();
    velocity = new PVector();
    acceleration = new PVector();
    
    dimensions = new PVector(20, 20);
    
  }
  
  
  

  void update(float deltaTime) {
    velocity.add(acceleration);
    location.add (velocity);
    
    acceleration.mult(0);
  }
  
  void display() {
    pushMatrix();
      translate(location.x, location.y);

      // Code diplay stuff here

      fill(fillColor);
      rect (0, 0, dimensions.x, dimensions.y);


              
    popMatrix();

  }
  

}
